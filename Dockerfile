###stage build

FROM maven:3.8.6-jdk-8
COPY . .
RUN mvn clean install package

#### stage RUN
FROM tomcat:8-jre8
COPY --from=0 webapp/target/webapp.war /usr/local/tomcat/webapps
